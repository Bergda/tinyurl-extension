chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
    if (message.action === "copyToClipboard") {
        // Create a temporary textarea element to copy the TinyURL to the clipboard
        const textarea = document.createElement("textarea");
        textarea.value = message.data;
        document.body.appendChild(textarea);
        textarea.select();
        document.execCommand("copy");
        document.body.removeChild(textarea);

        // Notify the background script that the URL has been copied
        sendResponse({ success: true });
    }
});
