const notifyUser = (message, color) => {
    chrome.action.setBadgeBackgroundColor({color: color});
    chrome.action.setBadgeText({ text: message });
    setTimeout(() => {
        chrome.action.setBadgeText({ text: "" });
    }, 2000); // Show the message in the badge text for 2 seconds
};

const generateTinyURL = async (url) => {
    return fetch(`https://tinyurl.com/api-create.php?url=${encodeURIComponent(url)}`)
            .then(response => response.text())
            .catch(() => notifyUser('ERR', 'red'));
};

const copyUrl = async (linkUrl) => {
    const [tab] = await chrome.tabs.query({ active: true, currentWindow: true });

    const url = linkUrl ? linkUrl : tab.url;
    
    // Generate the TinyURL
    const tinyURL = await generateTinyURL(url);

    // Inject the content script into the current tab
    chrome.scripting
        .executeScript({
            target: { tabId: tab.id },
            files: ['script.js']
        })
        .then(() => {
            chrome.tabs.sendMessage(tab.id, { action: "copyToClipboard", data: tinyURL }, (response) => {
                if (response && response.success) {
                    notifyUser('OK', 'green');
                } else {
                    notifyUser('ERR', 'red');
                }
            });
        })
        .catch(() => notifyUser('ERR', 'red'));
};

const onContextMenuClick = async (info) => {
    if (info.linkUrl) {
        copyUrl(info.linkUrl);
    } else {
        notifyUser('ERR', 'red');
    }
};

chrome.contextMenus.create({
    id: "shortenLink",
    title: "Shorten Link and Copy to Clipboard",
    contexts: ["link"],
});

chrome.contextMenus.onClicked.addListener(onContextMenuClick);

chrome.action.onClicked.addListener(async () => {  
    try {
        copyUrl();
    } catch {
        notifyUser('ERR', 'red');
    }
});
